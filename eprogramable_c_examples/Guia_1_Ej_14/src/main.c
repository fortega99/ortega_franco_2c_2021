/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*
 Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20 caracteres y edad.
Defina una variable con esa estructura y cargue los campos con sus propios datos.
Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso por punteros).

 */




/*==================[inclusions]=============================================*/
#include "../../Guia_1_Ej_14/inc/main.h"

#include "stdint.h"
#include "string.h"


/*==================[macros and definitions]=================================*/

struct Alumno {
	char Apellido [12];
	char Nombre [20];
	int Edad;
			  };



/*==================[internal functions declaration]=========================*/

int main(void)
{
    struct Alumno alumno_1, alumno_2;
    struct Alumno *aux;

    strcpy (alumno_1.Nombre, "Franco");
	strcpy (alumno_1.Apellido, "Ortega");
	alumno_1.Edad = 22;

	aux=& alumno_2;
	strcpy (aux->Nombre, "Luca");
	strcpy (aux->Apellido, "Perez");
	aux->Edad= 21;

	printf ("Mi nombre es: %s \r\n", alumno_1.Nombre);
	printf ("Mi apellido es: %s \r\n", alumno_1.Apellido);
	printf ("Mi edad es: %d \r\n", alumno_1.Edad);
	printf (" \r\n");
	printf ("El nombre de mi compañero es: %s \r\n", aux->Nombre);
	printf ("El apellido es: %s \r\n", aux->Apellido);
	printf ("Su edad es: %d \r\n", aux->Edad);


	return 0;
}

/*==================[end of file]============================================*/

