/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

#define TRUE 1
#define FALSE 0
#define ANCHO_VENTANA  231
#define TIEMPO_MUESTREO 4
#define TIEMPO_QRS 120
int i;
float ventana_datos [ ANCHO_VENTANA] ;
float ventana_diff [ ANCHO_VENTANA ] = {0};
float ventana_diff2 [ ANCHO_VENTANA ] = {0};
int hay_dato= FALSE;


unsigned char ecg[ANCHO_VENTANA]={
			76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
			89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
			99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
			94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
			88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
			83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
			79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
			80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
			86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
			94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
			180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
			89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
			94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
			103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
			100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
			99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
			74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
	};
float ventana_datos [ ANCHO_VENTANA] ;

/*==================[internal functions declaration]=========================*/


int main(void)
{

	int h;
		unsigned char Umbral;
		unsigned char valor=0;
			for (h=0; h < ANCHO_VENTANA ; h++)
			{
				if (valor < ecg [h])
					valor= ecg [h];
			}
			Umbral = valor;
			printf("El valor de Umbral es:%i\n", (int)Umbral);
	int contador=0, contador1=0 , contador2=0;
	for (i=1 ; i < ANCHO_VENTANA ; i+=1)
		{
			ventana_diff [i] = ( ecg [i]-ecg [i-1] )/ TIEMPO_MUESTREO;
			printf ("/%f",ventana_diff [i]);
			contador = contador+1;

		};
	printf("%f",contador);

printf ("\n");
		for (i=0 ; i < ANCHO_VENTANA ; i+=1)
		{
			ventana_diff2 [i] = ventana_diff [i] * ventana_diff [i];
			printf ("/%f",ventana_diff2 [i]);
			contador1 = contador+1;
		};
printf("%f",contador1);


/*	int h;
	char Umbral;
	char valor=0;
		for (h=0; i < ANCHO_VENTANA ; h++)
		{
			if (valor < ventana_diff2[h])
				valor=ventana_diff2 [h];
		}
		Umbral = valor;
		printf("El valor de Umbral es:%f\n", Umbral);
	int j;
		for (j=0; i < ANCHO_VENTANA ; j++)
		{
			if (ventana_diff[j] > Umbral/3)
			{
				hay_dato = TRUE;
						if (hay_dato == TRUE & contador2 < (TIEMPO_QRS/TIEMPO_MUESTREO))
							contador2++;
							continue;
			}
			printf("/%d", contador);

		}*/
		return 0;
}

/*==================[end of file]============================================*/

