/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
/*==================[macros and definitions]=================================*/

#define Ej17 1
#define cantidad 15
uint8_t numeros[15];
uint8_t promedio;
uint16_t sum; //Variable de 16 bits, que tendra la suma de los 15 numeros;

/*==================[internal functions declaration]=========================*/

int main(void)
{
    #ifdef Ej17
	int i;
	numeros [0]=234;
	numeros [1]=123;
	numeros [2]=111;
	numeros [3]=101;
	numeros [4]=32;
	numeros [5]=116;
	numeros [6]=211;
	numeros [7]=24;
	numeros [8]=214;
	numeros [9]=100;
	numeros [10]=124;
	numeros [11]=222;
	numeros [12]=1;
	numeros [13]=129;
	numeros [14]=9;

	for (i=0; i < cantidad ;i++)
		sum = sum + numeros[i];
	promedio = (sum/cantidad);
	printf("El promedio de los 15 numeros es: %d \r\n", promedio);
#endif
#ifndef Ej17
uint8_t numeros [16];
int cant =16,i;

	numeros [0]=234;
	numeros [1]=123;
	numeros [2]=111;
	numeros [3]=101;
	numeros [4]=32;
	numeros [5]=116;
	numeros [6]=211;
	numeros [7]=24;
	numeros [8]=214;
	numeros [9]=100;
	numeros [10]=124;
	numeros [11]=222;
	numeros [12]=1;
	numeros [13]=129;
	numeros [14]=9;
	numeros [15]=233;

	for (i=0; i < cant ;i++)
		sum = sum + numeros[i];
	promedio = sum>>4;
	printf("El promedio de los 16 numeros es: %d \r\n", promedio);




#endif
	return 0;
};
/*==================[end of file]============================================*/

