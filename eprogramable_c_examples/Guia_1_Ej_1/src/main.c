/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


//1-Declare una constante de 32 bits, con todos los bits en 0 y el bit 6 en 1. Utilice el operador <<.-
//2-Declare una constante de 16 bits, con todos los bits en 0 y los bits 3 y 4 en 1. Utilice el operador <<.
//3-Declare una variable de 16 bits sin signo, con el valor inicial 0xFFFF y luego, mediante una operación de máscaras, coloque a 0 el bit 14.
//4-Declare una variable de 32 bits sin signo, con el valor inicial 0x0000 y luego, mediante una operación de máscaras, coloque a 1 el bit 2.
//5-Declare una variable de 32 bits sin signo, con el valor inicial 0x00001234 y luego, mediante una operación de máscaras, invierta el bit 0.


/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>


/*==================[macros and definitions]=================================*/

#define BIT_6 6
#define BIT_4 4
#define BIT_3 3
#define BIT_14 14
#define BIT_2 2



const uint32_t VAR_32_BITS = (1<<BIT_6);
const uint16_t VAR_16_BITS = (1<<BIT_4)|(1<<BIT_3);
uint16_t VAR_16_BITS_01 =(0xFFFF)&~(1<<BIT_14);
uint32_t EJERCICIO_4 =(0x0000)|(1<<BIT_2);
uint32_t EJERCICIO_5 = (0x00001234);


/*==================[internal functions declaration]=========================*/

int main(void)
{
	printf("%d \r\n" , VAR_32_BITS);
	printf("%d \r\n" , VAR_16_BITS);
	printf("%d \r\n" , VAR_16_BITS_01);
	printf("%d \r\n" , EJERCICIO_4);
	printf("%d \r\n" , EJERCICIO_5);


	return 0;
}

/*==================[end of file]============================================*/

