/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
eclare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
Realice el mismo ejercicio, utilizando la definición de una “union”.


 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

#define BIT_8 8

uint32_t variable = 0x01020304;
uint32_t var = 0x01020304;

uint8_t var1, var2,var3,var4;
union dato
		{
			uint32_t numero;
			uint8_t num1;
			uint8_t num2;
			uint8_t num3;
			uint8_t num4;

		};



/*==================[internal functions declaration]=========================*/

int main(void)
{
   var4 = (uint8_t) variable;
   variable = variable>>BIT_8;
   printf ("Los bytes del 0 al 7 son: %x \r\n", var4);
   var3 = (uint8_t) variable;
   variable = variable>>BIT_8;
   printf ("Los bytes del 8 al 15 son: %x \r\n", var3);
   var2 = (uint8_t) variable;
   variable = variable>>BIT_8;
   printf ("Los bytes del 16 al 23 son: %x \r\n", var2);
   var1 = (uint8_t) variable;
   variable = variable>>BIT_8;
   printf ("Los bytes del 24 al 31 son: %x \r\n ", var1);
   printf ( "\r\n");
   union dato d;
   d.numero = var ;
   printf ("Los bytes del 0 al 7 son: %x \r\n", d.num1);
   printf ("Los bytes del 8 al 15 son: %x \r\n", d.num2);
   printf ("Los bytes del 16 al 23 son: %x \r\n", d.num3);
   printf ("Los bytes del 24 al 31 son: %x \r\n", d.num4);

   return 0;
   }

/*==================[end of file]============================================*/

