/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"


/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 2
#define TOGGLE 3

#define LED_1 1
#define LED_2 2
#define LED_3 3

//typedef enum leds_t {LED_1=1,LED_2,LED_3};

struct leds
{
	uint8_t n_led;        //indica el número de led a controlar
	uint8_t n_ciclos;     //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;      //indica el tiempo de cada ciclo
	uint8_t mode;         //ON=1, OFF=2, TOGGLE=3
} my_leds;



/*==================[internal functions declaration]=========================*/

void Control_Leds (struct leds *my_leds)
{
	int i,j;
	switch (my_leds->mode)
	{
	case ON:
			switch (my_leds->n_led)
				{
			case LED_1: printf ("El led 1 esta prendido\r\n");
			break;
			case LED_2: printf ("El led 2 esta prendido\r\n");
			break;
			case LED_3: printf ("El led 3 esta prendido\r\n");
			break;
				}
	break;
	case OFF:
		switch (my_leds->n_led)
						{
					case LED_1: printf ("El led 1 esta apagado\r\n");
					break;
					case LED_2: printf ("El led 2 esta apagado\r\n");
					break;
					case LED_3: printf ("El led 3 esta apagado\r\n");
					break;
						}
	break;
	case TOGGLE:

		for (i=0; i < my_leds->n_ciclos ;i++)
		{
			if (my_leds->n_led==LED_1)

					printf ("El led 1 esta parpadeando\r\n");

			else if (my_leds->n_led==LED_2)

					printf ("El led 2 esta parpadeando\r\n");

			else if (my_leds->n_led==LED_3 )

					printf ("El led 3 esta parpadeando\r\n");

			for (j=0; j< my_leds->periodo;j++)
				{
					printf ("Retardo.\r\n");
				}
		}
	break;
	}
};


int main(void)
{
	struct leds Prueba_1={3,4,5,3};
	Control_Leds (&Prueba_1);
	return 0;

}

/*==================[end of file]============================================*/

