/*! @mainpage Ultrasonido con interrupciones
 *
 * \section genDesc General Description
 *
 * This application makes a sensor with a display. In this proyect when you use TEC1 it turns the program On, when you use again turn the program Off.
 * When you use TEC2 the program hold the last value.
 *
 * \section hardConn Hardware Connection
 *
 * | Hc_sr4      	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	5,5V		|
 * | 	GND 	 	| 	GND 		|
 * | 	ECHO	 	| 	T_FIL		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 *
 * | DisplayITS_E0803  	|   EDU-CIAA	|
 * |:--------------:	|:--------------|
 * | 	VCC		 		| 	5,5V		|
 * | 	GND 	 		| 	GND 		|
 * | 	LCD1		 	| 	D1			|
 * | 	LCD2		 	| 	D2			|
 * | 	LCD3		 	| 	D3			|
 * | 	LCD4	 	 	| 	D4			|
 * | 	GPIO1		 	| 	SEL_0		|
 * | 	GPIO3		 	| 	SEL_1		|
 * | 	GPIO5		 	| 	SEL_2		|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Ortega Franco
 *
 */



/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "hc_sr4.h"
#include "stdio.h"
#include "stdint.h"
#include "gpio.h"
#include "switch.h"
#include "DisplayITS_E0803.h"
#include "led.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/
int16_t prueba;
int8_t teclas, tecla;
bool Prender = false;
bool Hold = true;


/*==================[internal data definition]===============================*/
void Medir();
timer_config my_timer = {TIMER_A,1000,&Medir};

/*==================[internal functions declaration]=========================*/
bool Tecla_1()
	{
	Prender = !Prender;
	return true;
	};
bool Tecla_2()
	{
	Hold = !Hold;
	return true;
	};
void Medir()
{
	if (Prender){
							prueba= HcSr04ReadDistanceCentimeters();
							//HcSr04Colores(prueba);
							//ITSE0803DisplayValue (prueba);
									if (Hold)
											{
											HcSr04Colores (prueba);
											ITSE0803DisplayValue (prueba);
											}
						}
			else {
				HcSr04Apagar_Leds ();
				ITSE0803DisplayValue (0);
				};
	};




/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/


int main(void){


	SystemClockInit();
	LedsInit();					//Inicializamos los LEDS
	SwitchesInit();				// Inicializamos Teclas
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	gpio_t pin_es[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	ITSE0803Init (pin_es);
	HcSr04Init(GPIO_T_FIL2 ,  GPIO_T_FIL3);
	SwitchActivInt(SWITCH_1, Tecla_1);
	SwitchActivInt(SWITCH_2, Tecla_2);
	while(1){

			};
	return 0;
			}

/*==================[end of file]============================================*/

