/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Infrarrojo.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "Tcrt5000.h"
#include "delay.h"
/*==================[macros and definitions]=================================*/
#define TIEMPOMS 100

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	//Ingreso pines a usar

	Tcrt5000Init(GPIO_T_COL0);
	bool estado;
	while(1){
		estado =Tcrt5000State();
		DelayMs(TIEMPOMS);
	}

	return 0;
}

/*==================[end of file]============================================*/

