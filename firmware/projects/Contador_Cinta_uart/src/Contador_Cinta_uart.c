/*! @mainpage Contador_Cinta
 *
 * \Contador de objetos en cinta transportadora

 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/9/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Contador_Cinta_uart.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "systemclock.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/

#define TIEMPOMS 200
//#define TIEMPOms 100
#define ON 1
#define OFF 0

bool TEC1=OFF;
bool TEC2=ON;
bool TEC3=OFF;
uint8_t teclas;
bool cinta_contadora=OFF;
int cantidad_obj=0;
uint8_t dato_teclado;
/*==================[internal data definition]===============================*/
void Medir();
void Teclado();
timer_config my_timer = {TIMER_A, 500, &Medir};
serial_config lectura ={SERIAL_PORT_PC,115200,&Teclado};

/*==================[internal functions declaration]=========================*/
void Led_Nat_A_Bin(uint8_t cantidad_obj){
	switch(cantidad_obj){
	// LED_RGB_B (Azul) como el bit 2**3, el LED_1 como 2**2, el LED_2 como 2**1 y el LED_3 como 2**0.

	case 0:
		LedsOffAll();
		break;
	case 1:
		LedOn(LED_3);

		break;
	case 2:
		LedsOffAll();
		LedOn(LED_2);


		break;
	case 3:
		LedsOffAll();
		LedOn(LED_3);
		LedOn(LED_2);


		break;
	case 4:
		LedsOffAll();
		LedOn(LED_1);

		break;
	case 5:
		LedsOffAll();

		LedOn(LED_1);
		LedOn(LED_3);


		break;
	case 6:
		LedsOffAll();

		LedOn(LED_1);
		LedOn(LED_2);


		break;
	case 7:
		LedsOffAll();

		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);

		break;
	case 8:
		LedsOffAll();

		LedOn(LED_RGB_B);




		break;
	case 9:
		LedsOffAll();

		LedOn(LED_RGB_B);
		LedOn(LED_3);


		break;
	case 10:
		LedsOffAll();

		LedOn(LED_RGB_B);
		LedOn(LED_2);
		break;
	case 11:
		LedsOffAll();

		LedOn(LED_RGB_B);
		LedOn(LED_3);
		LedOn(LED_2);
		break;
	case 12:
		LedsOffAll();

		LedOn(LED_RGB_B);
		LedOn(LED_1);

		break;
	case 13:
		LedsOffAll();

		LedOn(LED_RGB_B);
		LedOn(LED_3);
		LedOn(LED_1);
		break;
	case 14:
		LedsOffAll();

		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);

		break;
	case 15:
		LedsOffAll();

		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
		break;


	}
}

bool Tec1(){
	//PRENDO-APAGO
	TEC1=!TEC1;
	return true;
}
bool Tec2(){
	//HOLDEO
	TEC2=!TEC2;
	return true;
}
bool Tec3(){
	//RESET
	TEC3=!TEC3;
	return true;
}

void Medir(){


	if(TEC1 && cantidad_obj <16){
		cinta_contadora	= Tcrt5000State();
		//DelayMs(TIEMPOMS);
		if(cinta_contadora){
			cantidad_obj=cantidad_obj+1;
			//contadorG=cantidad_obj;
		}



		if(TEC2){
			//HOLDEO
			// envio valores a leds

			Led_Nat_A_Bin(cantidad_obj);


		}
		if(TEC3){
			//RESETEO
			cantidad_obj=0;
			//contadorG=cantidad_obj;
			LedsOffAll();
			TEC3=!TEC3;

		}
	}
	else{
		//VERIFICO EL APAGADO DE TEC1, PARPADEANDO EN ROJO RGB
		LedOn(LED_RGB_R);
		DelayMs(TIEMPOMS);
		LedOff(LED_RGB_R);
		DelayMs(TIEMPOMS);

		cantidad_obj=0;
		LedsOffAll();
	}
	UartSendString(&lectura, UartItoa(cantidad_obj, 10));
}

void Teclado(){
	UartReadByte(SERIAL_PORT_PC, &dato_teclado);
	switch(dato_teclado){
	case 'O':
		Tec1();
		break;
	case 'H':
		Tec2();
		break;
	case 'R':
		Tec3();
		break;
	}
}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	//Inits
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(GPIO_T_COL0);
	//gpio_t pin_es[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	//ITSE0803Init(pin_es);

	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	SwitchActivInt(SWITCH_1, Tec1);
	SwitchActivInt(SWITCH_2, Tec2);
	SwitchActivInt(SWITCH_3, Tec3);

	UartInit(&lectura);


	while(1){




	}


	return 0;

}




/*==================[end of file]============================================*/

