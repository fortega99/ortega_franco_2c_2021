/*! @mainpage Ultrasonido
 *
 * \section genDesc General Description
 *
 * This application makes a sensor with a display. In this proyect when you use TEC1 it turns the program On, when you use again turn the program Off.
 * When you use TEC2 the program hold the last value.
 *
 * \section hardConn Hardware Connection
 *
 * | Hc_sr4      	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	5,5V		|
 * | 	GND 	 	| 	GND 		|
 * | 	ECHO	 	| 	T_FIL		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 *
 * | DisplayITS_E0803  	|   EDU-CIAA	|
 * |:--------------:	|:--------------|
 * | 	VCC		 		| 	5,5V		|
 * | 	GND 	 		| 	GND 		|
 * | 	LCD1		 	| 	D1			|
 * | 	LCD2		 	| 	D2			|
 * | 	LCD3		 	| 	D3			|
 * | 	LCD4	 	 	| 	D4			|
 * | 	GPIO1		 	| 	SEL_0		|
 * | 	GPIO3		 	| 	SEL_1		|
 * | 	GPIO5		 	| 	SEL_2		|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Ortega Franco
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

