/*! @mainpage Proyecto_·
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Hc_sr4      	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	5,5V		|
 * | 	GND 	 	| 	GND 		|
 * | 	ECHO	 	| 	T_FIL		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 *
 * | DisplayITS_E0803  	|   EDU-CIAA	|
 * |:--------------:	|:--------------|
 * | 	VCC		 		| 	5,5V		|
 * | 	GND 	 		| 	GND 		|
 * | 	LCD1		 	| 	D1			|
 * | 	LCD2		 	| 	D2			|
 * | 	LCD3		 	| 	D3			|
 * | 	LCD4	 	 	| 	D4			|
 * | 	GPIO1		 	| 	SEL_0		|
 * | 	GPIO3		 	| 	SEL_1		|
 * | 	GPIO5		 	| 	SEL_2		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 23/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Ortega Franco
 *
 */

/*==================[inclusions]=============================================*/
#include "Proyecto_3.h"       /* <= own header */
#include "uart.h"
#include "systemclock.h"
#include "hc_sr4.h"
#include "stdio.h"
#include "stdint.h"
#include "gpio.h"
#include "switch.h"
#include "DisplayITS_E0803.h"
#include "led.h"
#include "timer.h"
/*==================[macros and definitions]=================================*/
int32_t prueba;
uint8_t base = 10;
uint8_t cantidad_bytes = 3;
int8_t tecla;
bool Prender = false;
bool Hold = true;
/*==================[internal data definition]===============================*/
void Medir();
void Leer_bytes();
timer_config my_timer = {TIMER_A,1000,&Medir};
serial_config UART_USB = {SERIAL_PORT_PC,115200, Leer_bytes};
/*==================[internal functions declaration]=========================*/
void Tecla_1()
	{
	Prender = !Prender;
	};
void Tecla_2()
	{
	Hold = !Hold;
	};
void Medir()
{
	if (Prender){
							prueba= HcSr04ReadDistanceCentimeters();
							//HcSr04Colores(prueba);
							//ITSE0803DisplayValue (prueba);
									if (Hold)
											{
											HcSr04Colores (prueba);
											ITSE0803DisplayValue (prueba);
											UartSendString (SERIAL_PORT_PC , UartItoa( prueba, base ));
											UartSendString (SERIAL_PORT_PC , (uint8_t*)" cm \r\n");

											}
						}
			else {
				HcSr04Apagar_Leds ();
				ITSE0803DisplayValue (0);
				UartSendString (SERIAL_PORT_PC , "0 cm \r\n");
				};
	};

void Leer_bytes()
{
	UartReadByte(SERIAL_PORT_PC, &tecla);
	switch (tecla){
					case 'O':
						Tecla_1();
						break;
					case 'H':
						Tecla_2();
						break;


};
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){


		SystemClockInit();
		UartInit(&UART_USB);
		LedsInit();					//Inicializamos los LEDS
		SwitchesInit();				// Inicializamos Teclas
		TimerInit(&my_timer);
		TimerStart(TIMER_A);
		gpio_t pin_es[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
		ITSE0803Init (pin_es);
		HcSr04Init(GPIO_T_FIL2 ,  GPIO_T_FIL3);
		SwitchActivInt(SWITCH_1, Tecla_1);
		SwitchActivInt(SWITCH_2, Tecla_2);
		while(1){

				};
		return 0;
				}
/*==================[end of file]============================================*/

