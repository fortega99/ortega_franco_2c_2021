/*! @mainpage Parcial_Ortega
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	---			| 	---			|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    	| Description                                |
 * |:--------------:|:-------------------------------------------|
 * | 01/11/2021	    | Document creation		                 	 |
 *
 * @author Ortega Franco
 *
 */

#ifndef _PARCIAL_ORTEGA_H
#define _PARCIAL_ORTEGA_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PARCIAL_ORTEGA_H */

