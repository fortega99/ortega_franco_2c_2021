/*! @mainpage Parcial_Ortega
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	-	 		| 	-			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Ortega Franco
 *
 */

/*==================[inclusions]=============================================*/
#include "Parcial_Ej_1_Ortega.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "timer.h"
#include "led.h"
#include "uart.h"

#include "hc_sr4.h"
#include "goniometro.h"



/*==================[macros and definitions]=================================*/
/** @def Intervalo
 * @brief Cada 10 ms se ejecutara la interrupcion por timer
 */
#define INTERVALO 10
/** @def ANG_75
 * @brief Para el angulo de 75 tengo 2100 mV
 */
#define ANG_75 2100
/** @def ANG_60
 * @brief Para el angulo de 60 tengo 1920 mV
 */
#define ANG_60 1920
/** @def ANG_45
 * @brief Para el angulo de 45 tengo 1740 mV
 */
#define ANG_45 1740
/** @def ANG_30
 * @brief Para el angulo de 30 tengo 1560 mV
 */
#define ANG_30 1560 /
/** @def ANG_15
 * @brief Para el angulo de 15 tengo 1380 mV
 */
#define ANG_15 1380
/** @def ANG_0
 * @brief Para el angulo de 0 tengo 1200 mV
 */
#define ANG_0  1200

uint16_t valor;
float medicion;
uint16_t distancia_medida75; // distancia medida para angulo de 75
uint16_t distancia_medida60; // distancia medida para angulo de 60
uint16_t distancia_medida45; // distancia medida para angulo de 45
uint16_t distancia_medida30; // distancia medida para angulo de 30
uint16_t distancia_medida15; // distancia medida para angulo de 15
uint16_t distancia_medida0; // distancia medida para angulo de 0

uint16_t Angulo_min;


/*==================[internal data definition]===============================*/


void SistInit();
void Angulo();
void Informe();


timer_config my_timer = {TIMER_A, INTERVALO , &Angulo};
timer_config my_timer2 = {TIMER_B,150*INTERVALO, &Informe};
serial_config val ={SERIAL_PORT_PC,115200,NULL};


/*==================[internal functions declaration]=========================*/
/** @fn Angulo ()
 * @brief Funcion para medir la distancia en función de los angulos
 * @param[in] No recibe
 * @return Null
 */
void Angulo()
{
	uint16_t distancia_minima = 50000; //defino una distancia minima de 50000 cm para que la primer medicion compare y fije un la primer medicion como valor minimo

	MedirPos();
		switch(	MedirPos())
	{
		case (ANG_75):
					{
						distancia_medida75= HcSr04ReadDistanceCentimeters();

						if (distancia_medida75 < distancia_minima)
							Angulo_min = 75;
					};
		case (ANG_60):
					{
						distancia_medida60= HcSr04ReadDistanceCentimeters();
						if (distancia_medida60 < distancia_minima)
							Angulo_min = 60;
					};
		case (ANG_45):
					{
						distancia_medida45= HcSr04ReadDistanceCentimeters();
						if (distancia_medida45 < distancia_minima)
							Angulo_min = 45;
					};
		case (ANG_30):
					{
						distancia_medida30= HcSr04ReadDistanceCentimeters();
						if (distancia_medida30 < distancia_minima)
							Angulo_min = 30;
					};
		case (ANG_15):
					{
						distancia_medida15= HcSr04ReadDistanceCentimeters();
						if (distancia_medida15 < distancia_minima)
							Angulo_min = 15;
					};
		case (ANG_0):
					{
						distancia_medida0= HcSr04ReadDistanceCentimeters();
						if (distancia_medida0 < distancia_minima)
							Angulo_min = 0;
					};
	};


};

/** @fn Informe ()
 * @brief Funcion para informar valores de distancia cada segundo y medio.
 * @param[in] No recibe
 * @return Null
 */
void Informe()
{
	UartSendString(SERIAL_PORT_PC ,"75º objeto a " );UartSendString(SERIAL_PORT_PC ,distancia_medida75 );UartSendString(SERIAL_PORT_PC ,".\n\r");
	UartSendString(SERIAL_PORT_PC ,"60º objeto a " );UartSendString(SERIAL_PORT_PC ,distancia_medida60 );UartSendString(SERIAL_PORT_PC ,".\n\r");
	UartSendString(SERIAL_PORT_PC ,"45º objeto a " );UartSendString(SERIAL_PORT_PC ,distancia_medida45 );UartSendString(SERIAL_PORT_PC ,".\n\r");
	UartSendString(SERIAL_PORT_PC ,"30º objeto a " );UartSendString(SERIAL_PORT_PC ,distancia_medida30 );UartSendString(SERIAL_PORT_PC ,".\n\r");
	UartSendString(SERIAL_PORT_PC ,"15º objeto a " );UartSendString(SERIAL_PORT_PC ,distancia_medida15 );UartSendString(SERIAL_PORT_PC ,".\n\r");
	UartSendString(SERIAL_PORT_PC ,"0º objeto a " );UartSendString(SERIAL_PORT_PC ,distancia_medida0 );UartSendString(SERIAL_PORT_PC ,".\n\r");
	UartSendString(SERIAL_PORT_PC , "Obstaculo en "); UartSendString(SERIAL_PORT_PC , Angulo_min);UartSendString(SERIAL_PORT_PC , "º.")




};
/** @fn SisInit ()
 * @brief Funcion para Inicializar el sistema
 * @param[in] No recibe
 * @return Null
 */
void SistInit()
{
	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2 ,  GPIO_T_FIL3);
	TimerInit(&my_timer);
	UartInit(&val);
	GonInit();
	LedsInit();

};
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	SistInit();
    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

