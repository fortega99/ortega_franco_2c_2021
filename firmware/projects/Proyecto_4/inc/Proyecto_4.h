/*! @mainpage TEMPLATE
 *
 * \section genDesc General Description
 *
 * El programa que se realizara constara de tomar la señal de un ECG, y a partir de una serie de procesos
 * graficar la señal tomada y mostrar la frecuencia cardiaca, tomando como valor normal un rango de 80-100 latidos por minutos
 * si se supera el mismo se mostrara en rojo con un texto que muestre la condición de "taquicardia" y si es menor al valor normal, mostrara
 * en rojo un texto con la condicion de "braquicardia".  Se tendra tambien la opción de pausar y reanudar el programa
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Ortega Franco
 *
 */

#ifndef _PROYECTO_4_H
#define _PROYECTO_4_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

