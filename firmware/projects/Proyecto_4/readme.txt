﻿Conexionado de Hardware: 
 * | PLACA DE BIOPOTENCIALES	|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	5V		 	| 	5V	|
 * | 	ECG		 	| 	CH1	|
 * | 	GND		 	| 	GND	|
 * | 	NC		 	| 	 -	|

 * |   	Display		|   EDU-CIAA	|
 * |:------------------:|:--------------|
 * | 	SDO/MISO 	|	SPI_MISO|
 * | 	LED		| 	3V3	|
 * | 	SCK		| 	SPI_SCK	|
 * | 	SDI/MOSI 	| 	SPI_MOSI|
 * | 	DC/RS	 	| 	GPIO_3	|
 * | 	RESET	 	| 	GPIO_5	|
 * | 	CS		| 	GPIO_1	|
 * | 	GND		| 	GND	|
 * | 	VCC		| 	3V3	|
