/*! @mainpage Proyecto_4
 *
 * \section genDesc General Description
 *

 * El programa que se realizara constara de tomar la señal de un ECG, y a partir de una serie de procesos
 * graficar la señal tomada y mostrar la frecuencia cardiaca, tomando como valor normal un rango de 50-120 latidos por minutos
 * si se supera el mismo se mostrara en rojo con un texto que muestre la condición de "taquicardia" y si es menor al valor normal, mostrara
 * en rojo un texto con la condicion de "bradicardia".
 * Se utilizara, mediante Hterm, una comunicación para informar el valor de frecuencia instantanea y los valores de frecuencia máxima y Mínima:
 * F: Frecuencia Instantanea
 * L: Frecuencia Mínima
 * H: Frecuencia Máxima
 *
 * \section hardConn Hardware Connection
 *
 * | PLACA DE BIOPOTENCIALES	|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	5V		 				| 	5V			|
 * | 	ECG		 				| 	CH1			|
 * | 	GND		 				| 	GND			|
 * | 	NC		 				| 	 -			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    					 	|
 * |:----------:|:----------------------------------------------------------------------|
 * | 01/10/2021 | Document creation		                        					 	|
 * | 15/10/2021	| Se empieza codigo de filtrado y detector     					    	|
 * | 21/10/2021	| Se completa codigo de filtrado  y detector y se añade driver ILI9341  |
 * | 22/10/2021	| Se modifica codigo							                     	|
 * | 25/10/2021	| Se agrega codigo para gráficar							                     					 	|
 * | 08/11/2021	| Se termina de modificar código y se agrega documentación							                     					 	|
 *
 * @author Ortega Franco
 *
 */

/*==================[inclusions]=============================================*/
#include "Proyecto_4.h"/* <= own header */
#include "uart.h"
#include "systemclock.h"
#include "analog_io.h"
#include "highpass.h"
#include "lowpass.h"
#include "iir.h"
#include "ili9341.h"
#include "gpio.h"
#include "realtimeplot.h"
#include "spi.h"
#include "delay.h"



#include "stdio.h"
#include "stdint.h"
#include "gpio.h"
#include "switch.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/

/** @def abs
 * @brief Función que calcula máximo
 */
#define abs(a) (((a) < (0)) ? (-a) : (a))
/** @def wL_HP NL_HP
 * @brief definiciones para filtros
 */
#define wL_HP NL_HP
/** @def wL_LP NL_LP
 * @brief Definiciones para filtros
 */
#define wL_LP NL_LP
/** @def ANCHO_VENTANA
 * @brief Ancho de datos de la ventana de ECG que se usara para la frecuencia
 */
#define ANCHO_VENTANA  1024
/** @def TIEMPO_MUESTREO
 * @brief Tiempo, en ms, cada cuanto se toma la muestra. Se tiene una frecuencia de muestreo de 250 Hz
 */
#define TIEMPO_MUESTREO 4
/** @def X
 * @brief Valor de 10 pixeles en X, que se utilizara para display
 */
#define X 10 //Valor en x del display
/** @def Y
 * @brief Valor de 10 pixeles en Y, que se utilizara para display
 */
#define Y 10 // valor en y del display
/** @def BRADICARDIA
 * @brief Valor minimo de frecuencia.
 */
#define BRADICARDIA 50
/** @def TAQUICARDIA
 * @brief Valor Maximo de Frecuencia
 */
#define TAQUICARDIA 120


double w_HP[wL_HP];   	// vector de estados internos del filtro pasa altos
double w_LP[wL_LP];   	// vector de estados internos del filtro pasa bajos

int i;
int j=0;
int a=0;
uint16_t output; // salida filtro pasa bajo
uint16_t output2; // salida filtro pasa alto
uint16_t dato=0; // Valor que se toma por CH1
uint16_t dato_frec =0; // dato pasado por filtro de frecuencia
float dato_deriv=0;  // dato pasado por filtro derivativo
float dato_cuadratico =0; // dato pasado por filtro cuadratico
float dato_anterior = 0; // dato para comparar en filtro derivativo
float umbral =0 ; // Valor que se utiliza como umbral para el calculo de la frecuencia
uint8_t tecla;
float ventana_filtrada [ANCHO_VENTANA];
uint8_t FC_MIN=120; // Designo un valor alto, para que la primer comparación funcione
uint8_t FC_MAX=40; // Designo un valor bajo, para que la primer comparación funcione
uint8_t Frecuencia_Cardiaca = 0;

float max; //valor maximo del vector de datos filtrados
float Umbral; //valor de umbral definido como el valor máximo sobre 3

void Detector (float *ventana_filt);
void SistInit ();
void LeerTecla();
void TomaDeDatos();
void ConversionDeDatos_Filtro();
void LeerTecla();




/*==================[internal data definition]===============================*/
analog_input_config Entrada_Analogica ={CH1 , AINPUTS_SINGLE_READ , &ConversionDeDatos_Filtro};
timer_config timer = {TIMER_A , TIEMPO_MUESTREO , &TomaDeDatos };
serial_config UART_USB = {SERIAL_PORT_PC,115200, &LeerTecla};
plot_t plot1 = {50,10,260,220,60,ILI9341_BLACK};
signal_t ecg = {10,20,ILI9341_WHITE,0,0};


/*==================[internal functions declaration]=========================*/


/** @fn TomaDeDatos ()
 * @brief Funcion para iniciar la conversion de datos analogicos
 * @param[in] No recibe
 * @return Null
 */
void TomaDeDatos()
{
	AnalogStartConvertion();

}

/** @fn ConcersionDeDatos_Filtro ()
 * @brief Funcion que lee el canal analogico, filtra la señal y  la guarda en un vector
 * @param[in] No recibe
 * @return Null
 */
void ConversionDeDatos_Filtro()
{

	w_LP[0] = 0;w_LP[1] = 0;w_LP[2] = 0;w_LP[3] = 0;w_LP[4] = 0;
	w_HP[0] = 0;w_HP[1] = 0;w_HP[2] = 0;w_HP[3] = 0;w_HP[4] = 0;
	if (j < ANCHO_VENTANA)
	{
		AnalogInputRead(CH1, &dato);

			output = 50*iir(DL_LP - 1, DEN_LP, NL_LP - 1, NUM_LP, w_LP, dato);    // Filtro pasa bajo

			output2 = iir(DL_HP - 1, DEN_HP, NL_HP - 1, NUM_HP, w_HP, output); // Filtro pasa alto
			RTPlotDraw (&ecg , output2); // Grafico dato a dato en la señal de ecg
			if (a==1)
			{
				dato_anterior = output2;
				a++;
			}
			else
			{
			/*		dato_deriv =  (float)(output2 - dato_anterior )/ (float)TIEMPO_MUESTREO;      // Filtro derivativo
					dato_anterior = output2;

					dato_cuadratico = dato_deriv * dato_deriv;                   // Filtro Cuadratico*/
					ventana_filtrada[a]=output2;   // Guardo datos filtrados en un vector
					a++;j++;
			};
	};
		if (j == (ANCHO_VENTANA-1))
									{
									Detector(ventana_filtrada);
									a=0;
									j=0;
									};
}

/** @fn LeerTecla ()
 * @brief Funcion que lee la tecla que se tipea por UART y realiza acciones
 * @param[in] No recibe
 * @return Null
 */
void LeerTecla()
{
	UartReadByte(SERIAL_PORT_PC, &tecla);
		switch (tecla){
						case 'F':
							UartSendString(SERIAL_PORT_PC,(uint8_t*)"Frecuencia Cardiaca: ");UartSendString(SERIAL_PORT_PC,UartItoa(Frecuencia_Cardiaca,10) ); UartSendString(SERIAL_PORT_PC,(uint8_t*)"lat/min.\r\n");
							if (Frecuencia_Cardiaca < BRADICARDIA)
							{
								UartSendBuffer(SERIAL_PORT_PC ,"Presenta Bradicardia.\r\n", 23 );
							}
							else if (Frecuencia_Cardiaca > TAQUICARDIA)
							{
								UartSendBuffer(SERIAL_PORT_PC ,"Presenta Taquicardia.\r\n", 23 );
							}
							break;
						case 'L':
							UartSendString(SERIAL_PORT_PC,(uint8_t*)"Frecuencia Minima: ");UartSendString(SERIAL_PORT_PC,UartItoa(FC_MIN,10) );UartSendString(SERIAL_PORT_PC,(uint8_t*)"lat/min.\r\n");

							break;
						case 'H':
							UartSendString(SERIAL_PORT_PC,(uint8_t*)"Frecuencia Maxima: ");UartSendString(SERIAL_PORT_PC,UartItoa(FC_MAX,10) );UartSendString(SERIAL_PORT_PC,(uint8_t*)"lat/min.\r\n");
							break;

}
};

/**@fn Detector(float *ventana_filt)
 * @brief  		Busca un umbral y detecta picos del mismo para informar la frecuencia cardiaca
 * @param[in]  	Puntero a vector con datos ya filtrados
 * @retval 		NULL
 */
void Detector (float *ventana_filt)
{
	int h;
	float valor_ini=0, valor_final=0;


		for (h=0; h < (ANCHO_VENTANA-1) ; h++)
		{
			if (max < ventana_filt [h])
				max = ventana_filt [h];
		}
		Umbral= (float) max* 0.8;
	int contador1=0;
	for (i=1; i < ANCHO_VENTANA-1 ; i++)
	{

		if ((ventana_filt[i] > Umbral) && (ventana_filt[i-1] <= Umbral ))
		{

					if (contador1==0)
						{
							valor_ini=i;
							contador1++;
						}

					else
					{
						valor_ini= valor_final;
						valor_final=i;
					};
					if (contador1 != 0){

						Frecuencia_Cardiaca = 4*(60*1000/((valor_final-valor_ini)*TIEMPO_MUESTREO)); //Calculo de frecuencia, se que 1Hz equivale a 60 lat/min, por regla de 3 tengo la expresión


						if (Frecuencia_Cardiaca<FC_MIN && Frecuencia_Cardiaca > 0)

							FC_MIN = Frecuencia_Cardiaca;

						if (Frecuencia_Cardiaca>FC_MAX)

							FC_MAX= Frecuencia_Cardiaca;

						valor_final = valor_ini;
					};

		}



	};
	if (Frecuencia_Cardiaca < BRADICARDIA)
	{
		ILI9341DrawInt(X,Y,Frecuencia_Cardiaca , 3 ,&font_11x18, ILI9341_RED,ILI9341_BLACK);
	}
	else if (Frecuencia_Cardiaca > TAQUICARDIA)
	{
		ILI9341DrawInt(X,Y,Frecuencia_Cardiaca , 3 ,&font_11x18, ILI9341_RED,ILI9341_BLACK);
	}
	else
	{
		ILI9341DrawInt(X,Y,Frecuencia_Cardiaca , 3 ,&font_11x18, ILI9341_WHITE,ILI9341_BLACK);
	}


	Umbral=0;
	max =0;
}

/** @fn SistInit ()
 * @brief Funcion para realizar inicializaciones
 * @param[in] No recibe
 * @return Null
 */
void SistInit()
{

	SystemClockInit();

	ILI9341Init(SPI_1,GPIO_1,GPIO_3,GPIO_5); //Inicializo pines para ILI9341

	ILI9341Rotate(ILI9341_Landscape_1);

	AnalogInputInit(&Entrada_Analogica); //Interrupción analogica

	TimerInit (&timer);    // Inicializo Timer

	UartInit(&UART_USB);  // Inicializo UART

	RTPlotInit(&plot1);   // Inicializo para graficar en pantalla

	RTSignalInit( &plot1 , &ecg); // Inicializo Señal

	TimerStart (TIMER_A); // Comienza a correr el timer

}



/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){


SistInit();

while (1){

}
    
	return 0;
}

/*==================[end of file]============================================*/

