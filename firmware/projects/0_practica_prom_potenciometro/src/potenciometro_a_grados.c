/*! @mainpage Contador_Cinta
 *
 * \Contador de objetos en cinta transportadora

 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * | 	Tcrt5000	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO0		|
 * |	Vcc			|		5 V		|
 * |	GND			|	GND			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/9/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 */

/*==================[inclusions]=============================================*/
#include "potenciometro_a_grados.h"       /* <= own header */
#include "analog_io.h"
#include "gpio.h"
#include "systemclock.h"

#include "delay.h"
#include "bool.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0
#define INTERVALO 100


#define MAX_VOLTAGE 3300 //3,3 V
#define MIN_VOLTAGE 0

#define MAX_VALUE 1024
#define MAX_GRADOS 180
#define GRADOS MAX_GRADOS/MAX_VOLTAGE

bool TEC1=OFF;
bool TEC2=ON;
bool TEC3=OFF;


bool conversion; /**< booleando si realiza o no la conversión*/
uint16_t valor; /**< Valor convertido*/
uint8_t canal; /**< Canal de la placa*/

int16_t maximo_actual=0;

int16_t cantidad_obj=0;
uint8_t dato_teclado;

/*==================[internal data definition]===============================*/
void Start();
void Teclado();
void InformoGrados();
void ReadValue();
float UnitConvert(uint16_t value);

analog_input_config potenciometro = {CH1,AINPUTS_SINGLE_READ,&ReadValue}; /**< Typedef configuración puerto analógico/digital canal x*/
timer_config my_timer = {TIMER_A, INTERVALO , &Start};
timer_config my_timer2= {TIMER_B,10*INTERVALO,&InformoGrados};
serial_config lectura ={SERIAL_PORT_PC,115200,&Teclado};

/*==================[internal functions declaration]=========================*/

void ReadValue(){
	conversion=false;
	AnalogInputRead(canal,&valor);
}
float UnitConvert(uint16_t value){

	float new_value;

	new_value = value;
	new_value = (float)new_value*MAX_VOLTAGE/MAX_VALUE;


	return new_value;
}

float ReadPotenciometro(){
	AnalogInputInit(&potenciometro);
	conversion = true;
	canal =CH1;
	AnalogStartConvertion();

	return UnitConvert(valor);
}

int16_t MaximoAnguloMedido(int16_t grados_medidos){

	if(maximo_actual<=grados_medidos){

		maximo_actual=grados_medidos;
	}
	else {
		return maximo_actual;
	}
	return maximo_actual;
}


bool Tec1(){
	//PRENDO-APAGO
	TEC1=!TEC1;
	return true;
}
bool Tec2(){
	//HOLDEO
	TEC2=!TEC2;
	return true;
}
bool Tec3(){
	//RESET
	TEC3=!TEC3;
	return true;
}

void InformoGrados(){
	int16_t grados_medidos = (int16_t)cantidad_obj*GRADOS;
	UartSendString(&lectura, UartItoa(grados_medidos, 10));
	UartSendString(SERIAL_PORT_PC, " ° ACTUAL\n\r");

	int16_t maximo=	MaximoAnguloMedido(grados_medidos);
	UartSendString(&lectura, UartItoa(maximo, 10));
	UartSendString(SERIAL_PORT_PC, " ° MAX\n\r");
}

void Start(){


	if(TEC1){
		//Inicio
		ReadPotenciometro();


		if(TEC2){
			//Apago
			TEC1=OFF;


		}
		if(TEC3){
			//RESETEO
			cantidad_obj=OFF;
			Tec3();

		}

	}

}

void Teclado(){
	UartReadByte(SERIAL_PORT_PC, &dato_teclado);
	switch(dato_teclado){
	case 'O':
		Tec1();
		break;
	case 'H':
		Tec2();
		break;
	case 'R':
		Tec3();
		break;
	}
}

void SisInit(void)
{
	//Inits
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	TimerInit(&my_timer);
	TimerInit(&my_timer2);
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
	UartInit(&lectura);
	SwitchActivInt(SWITCH_1, Tec1);
	SwitchActivInt(SWITCH_2, Tec2);
	SwitchActivInt(SWITCH_3, Tec3);

}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SisInit();

	return 0;

}




/*==================[end of file]============================================*/

