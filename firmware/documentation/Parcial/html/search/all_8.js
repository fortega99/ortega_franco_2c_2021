var searchData=
[
  ['hc_5fsr4',['Hc_SR4',['../group___hc___s_r4.html',1,'']]],
  ['hcsr04apagar_5fleds',['HcSr04Apagar_Leds',['../group___hc___s_r4.html#gad777c98bce22547d06fbdcd621360a74',1,'HcSr04Apagar_Leds(void):&#160;hc_sr4.c'],['../group___hc___s_r4.html#gad777c98bce22547d06fbdcd621360a74',1,'HcSr04Apagar_Leds(void):&#160;hc_sr4.c']]],
  ['hcsr04deinit',['HcSr04Deinit',['../group___hc___s_r4.html#ga01c64fb254735bfe8e8c6a6d77cfa906',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___hc___s_r4.html#ga01c64fb254735bfe8e8c6a6d77cfa906',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04init',['HcSr04Init',['../group___hc___s_r4.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___hc___s_r4.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04readdistancecentimeters',['HcSr04ReadDistanceCentimeters',['../group___hc___s_r4.html#ga65fc37ad619c9ccb3d6af9c61d70ef87',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c'],['../group___hc___s_r4.html#ga65fc37ad619c9ccb3d6af9c61d70ef87',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c']]],
  ['hcsr04readdistanceinches',['HcSr04ReadDistanceInches',['../group___hc___s_r4.html#gabecd1a82ed5d32a42e8366374d137b62',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c'],['../group___hc___s_r4.html#gabecd1a82ed5d32a42e8366374d137b62',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c']]],
  ['height',['height',['../structplot__t.html#ab8f22708bbe2634f9476cd5a7d8e4497',1,'plot_t::height()'],['../structorientation__properties__t.html#a82900153b217156f4cd8ad116114639e',1,'orientation_properties_t::height()']]],
  ['highbyte',['HighByte',['../ili9341_8c.html#aef10f880cc4161fd8dca2422fc2f04d0',1,'ili9341.c']]],
  ['hwpin',['hwPin',['../structdigital_i_o.html#a93d2e4a48daa464205632175fdb7288c',1,'digitalIO']]],
  ['hwport',['hwPort',['../structdigital_i_o.html#a79691c4619ba92dbf4859aaa6e006531',1,'digitalIO']]]
];
