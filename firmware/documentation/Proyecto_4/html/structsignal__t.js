var structsignal__t =
[
    [ "color", "structsignal__t.html#a1c250d9df690cb15a2b504c39f3aebb6", null ],
    [ "plot", "structsignal__t.html#acd4921f0319654d419cff2eff70e054e", null ],
    [ "x_prev", "structsignal__t.html#a1d8490f4564491d103e8230dcdf2fb90", null ],
    [ "y_offset", "structsignal__t.html#ae81588ea149bc2bd2c6fdf762bd2065e", null ],
    [ "y_prev", "structsignal__t.html#a1239e12c4140cc4b709494ca20ceb83e", null ],
    [ "y_scale", "structsignal__t.html#a984177f82cbf13d931c0987289e0accb", null ]
];