/* Copyright 2016,
 * Ortega Franco
 * francoagu32@gmail.com
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "led.h"
#include "analog_io.h"
#include "goniometro.h"


/*==================[macros and definitions]=================================*/
#define MAX_VOLTAJE 2100 // 2,2v
#define MIN_VOLTAJE 1200 // 1,2v
#define MAX_GRADOS 75
#define VALOR_MAX 1024 // 2^10


float medicion;
uint16_t valor;


/*==================[internal data declaration]==============================*/



/*==================[internal functions declaration]=========================*/

void ConversionDatos();

/*==================[internal data definition]===============================*/

analog_input_config Potenciometro = {CH1 ,AINPUTS_SINGLE_READ, &ConversionDatos};


/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool GonInit()
{

	AnalogInputInit(&Potenciometro);
	LedsInit();
	return true;

};
void ConversionDatos()
{
	AnalogStartConvertion();

};
float UnitConvertion(uint16_t value){
	float nuevo_valor;
	nuevo_valor =  (float)valor*MAX_VOLTAJE / VALOR_MAX;

	return nuevo_valor;
};

float MedirPos(){

		AnalogInputRead( CH1 , &valor );
		medicion = UnitConvertion (valor);
		Led_Rojo();
		if (medicion = MAX_VOLTAJE)
			Led_Verde();
		else if (medicion = MIN_VOLTAJE)
			Led_Rojo();
		return medicion;


};
void Led_Rojo()
{
	LedOn(LED_RGB_R);
	LedOff (LED_RGB_B);
	LedOff (LED_RGB_G);
	LedOff (LED_1);
	LedOff (LED_2);
	LedOff (LED_3);
};

void Led_Verde()
{
		LedOn(LED_RGB_G);
		LedOff (LED_RGB_R);
		LedOff (LED_RGB_B);
		LedOff (LED_1);
		LedOff (LED_2);
		LedOff (LED_3);
};


bool GonDeInit()
{
	return true;
}





/*==================[end of file]============================================*/
