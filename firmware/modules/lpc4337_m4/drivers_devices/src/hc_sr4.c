	/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "gpio.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "switch.h"
#include "DisplayITS_E0803.h"


/*==================[macros and definitions]=================================*/
uint32_t contador=0;
int16_t distancia;
#define CONSTANTE_CM 58
#define CONSTANTE_PULG 148
gpio_t pin_trigger;
gpio_t pin_echo;


/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


bool HcSr04Init(gpio_t echo, gpio_t trigger)

{
	/** Configuración de los GPIO*/

	pin_trigger = trigger;
	pin_echo = echo;
	GPIOInit (echo, GPIO_INPUT);
	GPIOInit (trigger, GPIO_OUTPUT);
	GPIOOff (pin_trigger);

	return true;

}

int16_t HcSr04ReadDistanceCentimeters(void)
{
	contador =0;
		GPIOOn (pin_trigger);
		DelayUs (10);
		GPIOOff (pin_trigger);

	while (GPIORead(pin_echo) == false)
			{

			}
	while (GPIORead(pin_echo) == true)
	{
		DelayUs (1);
		contador ++;
	}
	distancia = contador/CONSTANTE_CM;

	return distancia;
}


int16_t HcSr04ReadDistanceInches(void)
{
		GPIOOn (pin_trigger);
		DelayUs (10);
		GPIOOff (pin_trigger);

		while (pin_echo == 0)
				{
					GPIORead(pin_echo);
				}
		while (pin_echo == 1)
		{
			GPIORead (pin_echo);
			DelayUs (1);
			contador ++;
		}
		distancia = contador/CONSTANTE_PULG;

		return distancia;
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
{
	GPIODeinit ();
	return true;

}


bool HcSr04Colores (int16_t distancia)

{


	if (distancia<= 10)
		{
			LedOn (LED_RGB_B);
			LedOff (LED_RGB_R);
			LedOff (LED_RGB_G);
			LedOff (LED_1);LedOff (LED_2);LedOff (LED_3);
		}
	if (10< distancia && distancia <=20)
		{
			LedOn (LED_RGB_B);
			LedOn (LED_1);
			LedOff (LED_RGB_R);
			LedOff (LED_RGB_G);
			LedOff (LED_2);
			LedOff (LED_3);
		};
	if (20< distancia && distancia <=30)
	{
		LedOn (LED_RGB_B);
		LedOn (LED_1);
		LedOn (LED_2);
		LedOff (LED_RGB_R);
		LedOff (LED_RGB_G);
		LedOff (LED_3);
	}
	if (distancia > 30)
	{
		LedOn (LED_RGB_B);
		LedOn (LED_1);
		LedOn (LED_2);
		LedOn (LED_3);
		LedOff (LED_RGB_R);
		LedOff (LED_RGB_G);
	}
	return true;

}

bool HcSr04Apagar_Leds(void)
{
	LedOff (LED_RGB_B);
	LedOff (LED_RGB_R);
	LedOff (LED_RGB_G);
	LedOff (LED_1);
	LedOff (LED_2);
	LedOff (LED_3);

	return true;


}

/*==================[end of file]============================================*/
