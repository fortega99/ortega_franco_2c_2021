/* Copyright 2016,
 * Ortega Franco
 * francoagu32@gmail.com
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef goniometro
#define goniometro


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Goniometro
 ** @{ */

/** @brief Bare Metal header for leds on EDU-CIAA NXP
 **
 **
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	FO			Franco Ortega
*/




/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/






/*==================[external functions declaration]=========================*/

/** @fn bool GonInit(gpio_t Divisor);
 * @brief Función de inicialición de potenciometro
 * @param[in] Pin de entrada del divisor.
 * @return TRUE if no error
 */
bool GonInit();

/** @fn ConversionDatos ()
 * @brief Funcion para convertir datos analogicos
 * @param[in] No recibe
 * @return Null
 */
void ConversionDatos();

/** @fn UnitConvertion (uint16_t valor)
 * @brief Funcion para convertir el valor de entrada en un valor digital, analogo al voltaje
 * @param[in] valor a convertir
 * @return Devuelve el voltaje
 */
float UnitConvertion(uint16_t value);

/** @fn MedirPos()
 * @brief Funcion para leer y convertir datos de izquierda a derecha
 * @param[in] Null
 * @return Valor de voltaje medido
 */
float MedirPos();
/** @fn Led_Rojo()
 * @brief Funcion para prender led rojo
 * @param[in] Null
 * @return Null
 */
void Led_Rojo();
/** @fn Led_Verde()
 * @brief Funcion para prender led verde
 * @param[in] Null
 * @return Null
 */
void Led_Verde();

/** @fn GonDeInit ()
 * @brief De Init de goniometro
 * @param[in] nada
 * @return TRUE si no encuentra error
 */
bool GonDeInit();







#endif
