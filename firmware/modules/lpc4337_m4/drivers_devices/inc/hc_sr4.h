/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef hc_sr4
#define hc_sr4


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Hc_SR4
 ** @{ */

/** @brief Bare Metal header for leds on EDU-CIAA NXP
 **
 **
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	FO			Franco Ortega
*/




/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/






/*==================[external functions declaration]=========================*/

/** @fn bool HcSr04Init(gpio_t echo, gpio_t trigger);
 * @brief Función de inicialición de puertos
 * Mapping ports (PinMux function), set direction and initial state of leds ports
 * @param[in] Parametros de entrada (Echo) y salida (Trigger)
 * @return TRUE if no error
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @fn HcSr04ReadDistanceCentimeters (void)
 * @brief Funcion para leer la distancia a la que se encuentra el objeto
 * @param[in] No recibe
 * @return Devuelve la distancia en centimetros
 */
int16_t HcSr04ReadDistanceCentimeters(void);

/** @fn HcSr04ReadDistanceInches (void)
 * @brief Función para leer la distancia a la que se encuentra el objeto
 * @param[in] No recibe
 * @return Devuelve la distancia en pulgadas
 */
int16_t HcSr04ReadDistanceInches(void);

/** @fn HcSr04Deinit (gpio_t echo, gpio_t trigger)
 * @brief Funcion para desinicializar los puertos
 * @param[in] Puertos de entrada (Echo) y salida (trigger)
 * @return TRUE si no encuentra error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/** @fn HcSr04Apagar_Leds ()
 * @brief Funcion para apagar leds y pantalla
 * @param[in] nada
 * @return TRUE si no encuentra error
 */
bool HcSr04Apagar_Leds(void);







#endif
